Pod::Spec.new do |s|

    s.name                  = 'TransaktSDK'
    s.version               = '3.0.1'
    s.summary               = 'Basic pod for TransaktSDK'
    s.homepage              = 'https://gitlab.com/entersekt/repos/SDK-IOS/TransaktSDK'

    s.author                = { 'Philip Nel' => 'pnel@entersekt.com.com' }
    s.license               = { :type => 'MIT' }
    s.source                = { :http => 'https://gitlab.com/philip.nel.ent/transakt-cocoapod/raw/master/transakt.tar.gz'}
    s.source_files          = '**/TransaktSDK.framework/Headers/*.h'
    s.vendored_frameworks   = '**/TransaktSDK.framework'
    s.platform              = :ios
    s.ios.deployment_target = '10.0'
    
end